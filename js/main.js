//myscript.js
window.onload = function(){
	ggg();
}
function ggg(){

	//ボールをたくさん生成したいので配列を宣言しておく
	var balls = [];

	var canvas = document.getElementById('canvas');

	//canvas自体を取れなかった場合、またはgetContextできなかった場合、処理を止める。
	if(!canvas || !canvas.getContext) return false;

	//getContextメソッドで描画機能を有効にする。
	var ctx = canvas.getContext('2d');


//生成されるBallオブジェクトを作成する関数
var Ball = function(x,y,r){

	//thisとはBallオブジェクトの変数ということ
	this.x = x;
	this.y = y;
	this.r = r;
	//ballの動きを乱数で設定
	this.vx = rand(-10,10);
	this.vy = rand(-10,10);

	this.color = "rgba("
					+rand(0,255) + "," +
					rand(0,255) + "," +
					rand(0,255) + "," +
					Math.random() +
					")";

	this.draw = function(){
		ctx.beginPath();
		ctx.arc(this.x,this.y,this.r,0,2*Math.PI);

		ctx.fillStyle = this.color;

		ctx.closePath();
		ctx.fill();
	};

	//ボールに動きをつけるメソッド
	this.move = function(){

		//this.vxがcanvasの横幅以上または0以下になった時、移動を制御するvxを反転させる。半径を考慮してrをプラスする。
		if(this.x + r > canvas.width || this.x - r < 0){
			this.vx = -this.vx;
		}
		//this.vyがcanvasの高さ以上または0以下になった時、移動を制御するvyを反転させる。
		if(this.y + r > canvas.height || this.y - r < 0){
			this.vy = -this.vy;
		}

		this.x += this.vx;
		this.y += this.vy;

	};

};//end of Ball Object
//var b = new Ball(100,100,20);
//b.draw();

$("#canvas").click(function(e){
	var x = e.pageX - $(this).offset().left;
	var	y = e.pageY - $(this).offset().top;
	var	r = rand(5,40);

	x = ajustPostion(x,r,canvas.width);
	y = ajustPostion(y,r,canvas.height);

	//円の初期x座標と円の半径を足した値が、canvas.widthより大きかったら、
	//初期x座標をcanvas.width - rにする。
	//if(x + r > canvas.width) x = canvas.width - r;

	//円の初期x座標から円の半径を引いた値が、0より小さかったら
	//初期x座標をrにする。
	//if(x - r < 0) x = r;

	//円の初期y座標と円の半径を足した値がcanvas.heightより大きかったら
	//初期y座標をcanvas.height - rにする。
	//if(y + r > canvas.height) y = canvas.width - r;

	//円の初期y座標から円の半径を引いた値が、0より小さかったら
	//初期y座標をrにする。
	//if(y - r < 0) y = r;

	function ajustPostion(pos,r,max){
		if(pos + r > max){
			return pos = max - r;
		}else if(pos - r < 0){
			return pos = r;
		}else{
			return pos;
		}
	}

	//配列にBallオブジェクトを格納していく
	balls.push(new Ball(x,y,r));
	console.log(balls);
});

//canvasを背景色で塗りつぶすメソッド
function clearStage(){
	ctx.fillStyle = "#ecf0f1";
	ctx.fillRect(0,0,canvas.width,canvas.height);
}

//ボールの動きを0.03秒ごとに更新して再描画することで再現する。
function update(){
	clearStage();

	for(var i = 0;i < balls.length; i++){
		balls[i].draw();
		balls[i].move();
	}
	setTimeout(update, 30);
}

update();

//乱数を発生させる関数
function rand(min,max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

}//end of ggg();